# nytgames

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Letter Boxed

Un [jeu du NYTimes](https://www.nytimes.com/puzzles/letter-boxed).

![demo](./letter-boxed-demo.gif)

> Exemple de solution : JAW - WORLD - DEADLY - YANK - KYLE - ELUDE

## Règles du jeu

- Il faut créer des mots en utilisant des lettres imposées,
  réparties autour d'un carré.
- Les mots doivent faire 3 lettres ou plus.
- Une lettre peut être réutilisée dans un même mot, et dans
  des mots différents.
- Il n'est pas possible d'utiliser deux lettres d'un même coté
  du carré à la suite.
- La dernière lettre d'un mot validé devient la première lettre
  du mot suivant.
- On peut supprimer une lettre à tout moment. Si un mot vient
  juste d'être validé, il est invalidé et on peut par conséquent
  le modifier.
- Le jeu se termine quand toutes les lettres ont été utilisées !

On peut jouer au clavier ou à la souris. Au clavier, il suffit de taper les lettres puis de faire entrer pour proposer de valider un mot. À la souris, il faut cliquer sur les lettres, puis sur _Enter_ pour proposer de valider un mot. On peut supprimer une lettre avec la touche Retour en arrière (_backspace_) ou le bouton _Delete_.

## Objectifs de l'exercice

- Implémenté avec React, en TypeScript.
- Le rendu graphique final importe peu, tant qu'il est clair que les lettres sont réparties autour d'un carré (pour bien visualiser les suites de lettres interdites). Idéalement, on essayera tout de même de coller au plus près de la maquette graphique du NYTimes.
- La partie centrale (les lignes reliant les lettres) est optionnelle. Le plus simple pour la gérer est d'utiliser un Canvas SVG. Un service de dessin de lignes en SVG est fourni dans src/services.
- Pas la peine, dans cette version, d'autoriser tous les mots anglais possible : il suffit d'autoriser les six mots proposés dans l'exemple ci-dessus pour valider que l'application fonctionne !

## Bonus

### :star: Ajouter un score

Grille de base :

- 9999 points si résolu en un seul mot (impossible :D)
- 500 points si résolu en deux mots
- 400 points si résolu en trois mots
- 300 points si résolu en quatre mots
- 200 points si résolu en cinq mots
- 100 points si résolu en six mots

Ajouter des points bonus en fonction de la longueur du mot validé : 50 points si 3 lettres, 25 points si 4 lettres, rien au-delà.

### :star::star: Utiliser un dictionnaire de mot distant

Utiliser un dictionnaire pour vérifier les mots proposés. Le dictionnaire peut-être un simple (mais gros !) fichier, ou une API.

- [Dictionnaire (fichier) Anglais](https://github.com/dwyl/english-words)
- [API multilingue](https://tech.yandex.com/dictionary/)



### :star::star: Support multilingue

Envisager de supporter plusieurs langues, avec la possibilité de passer de l'une à l'autre (seulement lorsqu'on démarre une nouvelle partie).


import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/actions/drawPointAction.dart';
import 'package:nytgames/actions/wordAction.dart';
import 'package:nytgames/events/nytGameEvent.dart';
import 'package:nytgames/models/appState.dart';
import 'package:nytgames/models/drawPointModel.dart';
import 'package:nytgames/models/pointModel.dart';
import 'package:nytgames/models/squarePointModel.dart';
import 'package:nytgames/models/wordModel.dart';
import 'package:touchable/touchable.dart';
import 'package:nytgames/actions/squareAction.dart';


class NytDiagram extends CustomPainter {
   final BuildContext context;
  final utils;
  final store;

  NytDiagram(this.context, this.utils, this.store);

  void _drawOldPoint(myCanvas, store,drawPoint) {
    var foundDrawPointStoredOld = store.state.drawPoint.where((e) =>
    e.isValidated == true);
    if (foundDrawPointStoredOld.length > 0) {
      for (var i = 0; i < drawPoint.length; i++) {
        for(var j = 0; j <= drawPoint[i].point.length; j++) {
          if (drawPoint[i].point.length > j + 1) myCanvas.drawLine(
              drawPoint[i].point[j].point, drawPoint[i].point[j + 1].point,
              utils.PaintChangeColor(Colors.pink[50], 3.0));
        }
      }
    }
  }





  @override
  void paint(Canvas canvas, Size size) {
    // TODO: draw something with canvas
    var myCanvas = TouchyCanvas(context, canvas);
    var drawPoint = this.store.state.drawPoint;
    //draw old point
    _drawOldPoint(myCanvas, this.store, drawPoint);
  }




  @override
  bool shouldRepaint(NytDiagram oldDelegate) => true;
}
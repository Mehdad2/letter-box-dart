
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/actions/drawPointAction.dart';
import 'package:nytgames/actions/wordAction.dart';
import 'package:nytgames/events/nytGameEvent.dart';
import 'package:nytgames/models/appState.dart';
import 'package:nytgames/models/drawPointModel.dart';
import 'package:nytgames/models/pointModel.dart';
import 'package:nytgames/models/squarePointModel.dart';
import 'package:nytgames/models/wordModel.dart';
import 'package:touchable/touchable.dart';
import 'package:nytgames/actions/squareAction.dart';




class NytGamePainter extends CustomPainter {

  final BuildContext context;
  final tap;
  final responseOk;
  nytGameEvent colorsPaint = nytGameEvent(Colors.pink);
  Color colorUpdated;
  final utils;
  final store;

  NytGamePainter(this.context, this.utils, this.tap, this.store, this.responseOk);

  String _getLetter(store,key) {

    var foundDrawLetter = store.state.drawLetter.where((e) => e.succeed == false);
    for( var l in foundDrawLetter)
      {
        return l.letters[key];
      }
  }

  void _drawOldPoint(myCanvas, store,drawPoint) {
    var foundDrawPointStoredOld = store.state.drawPoint.where((e) =>
    e.isValidated == true);
    if (foundDrawPointStoredOld.length > 0) {
      for (var i = 0; i < drawPoint.length; i++) {
        for(var j = 0; j <= drawPoint[i].point.length; j++) {
          if (drawPoint[i].point.length > j + 1) myCanvas.drawLine(
              drawPoint[i].point[j].point, drawPoint[i].point[j + 1].point,
              utils.PaintChangeColor(Colors.pink[50], 3.0));
        }
      }
    }
  }

  void _storePoint(store, t, side) {
    var foundDrawPoint = store.state.drawPoint.where((e) =>
    e.count != 0 && e.isValidated == false);
    if (foundDrawPoint.length > 0) {
      var length = store.state.drawPoint.length;
      for (var element in foundDrawPoint) {
        if (element.count > 0 && element.lastSide != side) {
          store.dispatch(updateDrawPointAction(length - 1, length,
              drawPointModel(
                  store.state.drawPoint[length - 1].point..add(pointModel(t.localPosition)),
                  false, store.state.drawPoint[length - 1].count + 1, side)));
        } else {
          store.dispatch(updateDrawPointAction(length - 1, length,
              drawPointModel(store.state.drawPoint[length - 1].point
                ..removeLast()
                ..add(pointModel(t.localPosition)), false,
                  store.state.drawPoint[length - 1].count + 1, side)));
        }
      };
    } else {
      store.dispatch(addDrawPointAction(drawPointModel([pointModel(t.localPosition)],false, 1, side)));
    }
  }

  void _managerDrawer(store, key, side, t, [color = Colors.amber]) {
    if (store.state.squareDots[key].count == 0 &&
        key == store.state.squareDots[key].indexColor) {
      store.dispatch(RemoveSquareAction(key));
      store.dispatch(InsertSquareAction(
          squarePointModel(key, Colors.green, 1, side, true)));
      _storePoint(store, t, side);

    }

    var foundElements = store.state.squareDots.where((e) =>
    e.count == 1 && e.indexColor != key
        //&& e.side == side
    );
    if (foundElements.length > 0) {
      store.dispatch(RemoveSquareAction(foundElements.first.indexColor));
      store.dispatch(InsertSquareAction(squarePointModel(
          foundElements.first.indexColor, color, 0, side, false)));
    }
  }

  void _managerWord(store,key, side)
  {
    var length = store.state.words.length;
    var letter = _getLetter(store,key);

    var foundElements = store.state.words.where((e) => e.isValidated == false);

    if(foundElements.length == 0) {
      store.dispatch(addWordAction(wordModel([letter], false, side)));
    } else {
      //warning we can not have two same letters consecutively;
      print(store.state.words[length - 1].side == side);
      if(store.state.words[length - 1].word.last == letter) {
        return;
      }
        if(store.state.words[length - 1].word.length > 0 && store.state.words[length - 1].side == side) {

         store.state.words[length - 1].word.removeLast();
        }
        store.dispatch(UpdateWordAction(length - 1, length, wordModel(store.state.words[length - 1].word..add(letter), false, side)));

    }

    if(foundElements.length != 0 && store.state.words.length > 0) {
      print(store.state.words[length - 1].word);
    }



  }


  @override
  void paint(Canvas canvas, Size size) {
    // TODO: draw something with canvas
    //final store = StoreProvider.of<AppState>(context);


    var length = this.store.state.drawPoint.length;
    var drawPoint = this.store.state.drawPoint;
    List<double> position = [
      size.width / 4,
      size.width / 2,
      size.width - size.width / 4
    ];


    var myCanvas = TouchyCanvas(context, canvas);
    final paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0
      ..color = Colors.indigo;

    //draw old point
    _drawOldPoint(myCanvas, this.store,drawPoint);



    myCanvas.drawRRect(
      RRect.fromRectAndRadius(
          Rect.fromLTWH(0, 0, size.width, size.width), Radius.circular(0)),
      paint,
    );

    position.asMap().forEach((key, value) {



      //top
      myCanvas.drawOval(
          Rect.fromLTWH(value, -5, 10, 10),
          utils.PaintChangeColor(this.store.state.squareDots[key].colors),
          onTapDown: (tapdetail) {
            _managerWord(this.store, key, 'top');
            _managerDrawer(this.store, key, 'top', tapdetail);
          }
      );

        //write letter top
      TextPainter(
          text: TextSpan(text:_getLetter(this.store,key), style: TextStyle(color: Colors.indigo)),
          textDirection: TextDirection.ltr,
        )..layout(
          minWidth: 100,
          maxWidth: 100,
        )..paint(canvas, Offset(value, - 35));




      //right
      myCanvas.drawOval(
        Rect.fromLTWH(size.width - 5, value, 10, 10),
        utils.PaintChangeColor(this.store.state.squareDots[key + 3].colors),
        onTapDown: (tapdetail) {
          _managerWord(this.store, key + 3, 'right');
          _managerDrawer(this.store, key + 3, 'right', tapdetail);
        },
      );

      //write letter right
      TextPainter(
        text: TextSpan(text:_getLetter(this.store,key + 3), style: TextStyle(color: Colors.indigo)),
        textDirection: TextDirection.ltr,
      )..layout(
        minWidth: 100,
        maxWidth: 100,
      )..paint(canvas, Offset(size.width + 25, value));

      //bottom
      myCanvas.drawOval(
        Rect.fromLTWH(value, size.width - 5, 10, 10),
        utils.PaintChangeColor(this.store.state.squareDots[key + 6].colors),
        onTapDown: (tapdetail) {
          _managerWord(this.store, key + 6, 'bottom');
          _managerDrawer(this.store, key + 6, 'bottom', tapdetail);
        },
      );
      //write letter bottom
      TextPainter(
        text: TextSpan(text:_getLetter(this.store,key + 6), style: TextStyle(color: Colors.indigo)),
        textDirection: TextDirection.ltr,
      )..layout(
        minWidth: 100,
        maxWidth: 100,
      )..paint(canvas, Offset(value, size.width + 25));

      //left
      myCanvas.drawOval(
        Rect.fromLTWH(-5, value, 10, 10),
        utils.PaintChangeColor(this.store.state.squareDots[key + 9].colors),
        onTapDown: (tapdetail) {
          _managerWord(this.store, key + 9, 'left');
          _managerDrawer(this.store, key + 9, 'left', tapdetail);
        },
      );

      //write letter left
      TextPainter(
        text: TextSpan(text:_getLetter(this.store,key + 9), style: TextStyle(color: Colors.indigo)),
        textDirection: TextDirection.ltr,
      )..layout(
        minWidth: 100,
        maxWidth: 100,
      )..paint(canvas, Offset(-30, value));

    });


    //draw  new point
    var foundDrawPointStoredNew = this.store.state.drawPoint.where((e) =>
    e.isValidated == false);
    if (foundDrawPointStoredNew.length > 0) {
      for (var i = 0; i <= drawPoint[length-1].point.length; i++) {
        if(drawPoint[length-1].point.length > i + 1) myCanvas.drawLine(drawPoint[length-1].point[i].point, drawPoint[length-1].point[i + 1].point,
            utils.PaintChangeColor(Colors.pink, 3.0));
      }
    }







  }


  @override
  bool shouldRepaint(NytGamePainter oldDelegate) => oldDelegate.tap != this.tap || oldDelegate.responseOk != this.responseOk;
}
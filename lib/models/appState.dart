import 'package:flutter/material.dart';
import 'package:nytgames/models/drawLetterModel.dart';
import 'package:nytgames/models/drawPointModel.dart';
import 'package:nytgames/models/letterModel.dart';
import 'package:nytgames/models/pointModel.dart';
import 'package:nytgames/models/squarePointModel.dart';
import 'package:nytgames/models/wordModel.dart';

class AppState {

  final List<squarePointModel> squareDots;
  final List<drawPointModel> drawPoint;
  final List<drawLetterModel> drawLetter;
  final List<wordModel> words;
  final int counter;


  AppState(this.squareDots,this.drawPoint,this.drawLetter, this.words, this.counter);

  AppState.initialState()
      : squareDots = List.unmodifiable(<squarePointModel> [squarePointModel(0,Colors.amber, 0, 'top', false)]..addAll(
      {
        squarePointModel(1,Colors.amber,0, 'top', false),
        squarePointModel(2,Colors.amber, 0, 'top', false),
        squarePointModel(3,Colors.amber, 0, 'right', false),
        squarePointModel(4,Colors.amber, 0, 'right', false),
        squarePointModel(5,Colors.amber, 0, 'right', false),
        squarePointModel(6,Colors.amber, 0, 'bottom', false),
        squarePointModel(7,Colors.amber, 0, 'bottom', false),
        squarePointModel(8,Colors.amber, 0,'bottom', false),
        squarePointModel(9,Colors.amber, 0, 'left', false),
        squarePointModel(10,Colors.amber, 0, 'left', false),
        squarePointModel(11,Colors.amber, 0, 'left', false),

      }
        )
    ),
        drawPoint =  new List.unmodifiable(<drawPointModel>[]) ,
        drawLetter = new List.unmodifiable(<drawLetterModel>[
          drawLetterModel('YDWOLAUKRENJ', 1, false),
        //  drawLetterModel('ABCDEFGHIJKL', 1, false)

        ]),
        words = new List.unmodifiable(<wordModel> []),

        counter = 0;

}
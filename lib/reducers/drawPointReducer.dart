import 'package:flutter/cupertino.dart';
import 'package:nytgames/actions/drawPointAction.dart';
import 'package:nytgames/models/drawPointModel.dart';

List<drawPointModel> addDrawPoint(List<drawPointModel> drawPoint, addDrawPointAction action) => List.from(drawPoint)..add(action.dotsItem);

//List<drawPointModel> removeDrawPoint(List<drawPointModel> drawPoint, action) =>  List.from(drawPoint);

//List<drawPointModel> removeDrawPoint(List<drawPointModel> drawPoint, action) => List.from(drawPoint)..remo;

List<drawPointModel> updateDrawPoint(List<drawPointModel> drawPoint, updateDrawPointAction action) => List.from(drawPoint)
  ..replaceRange(action.start, action.end, {action.dotsItem});



drawPointReducer(List<drawPointModel> drawPoint, action) {
  if(action is addDrawPointAction) {
    return addDrawPoint(drawPoint, action);
  } else if(action == DrawPointAction.removeLast) {
   // return removeDrawPoint(drawPoint, action);
  } else if(action is updateDrawPointAction) {
    return updateDrawPoint(drawPoint, action);
  }else {
    return drawPoint;
  }
}
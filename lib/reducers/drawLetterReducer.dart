import 'package:nytgames/models/drawLetterModel.dart';
import 'package:nytgames/actions/drawLetterAction.dart';

List<drawLetterModel> addDrawLetter(List<drawLetterModel> drawLetter, addDrawLetterAction action) => List.from(drawLetter)..add(action.letterItems);


drawLetterReducer(List<drawLetterModel> drawLetter, action) {
  if(action is addDrawLetterAction) {
    return addDrawLetter(drawLetter, action);
  } else {
    return drawLetter;
  }

}
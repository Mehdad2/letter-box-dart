import 'package:nytgames/actions/addCounter.dart';

int addCounterReducer(int counterInd,  action) {
  if(action == Counter.incrementCounter) {
    return counterInd + 1;
  } else if( action == Counter.decrementCounter) {
     return counterInd - 1;
  } else {
    return counterInd;
  }


}



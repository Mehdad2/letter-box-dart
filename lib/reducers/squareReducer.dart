import 'package:nytgames/models/squarePointModel.dart';
import 'package:nytgames/actions/squareAction.dart';

List<squarePointModel> addColorReducer(List<squarePointModel> colors , AddSquareAction action) =>  new List.from(colors)..add(action.color);

List<squarePointModel> removeColorReducer(List<squarePointModel> colors , RemoveSquareAction action) =>  new List.from(colors)..removeAt(action.indexColor);

List<squarePointModel> insertColorReducer(List<squarePointModel> colors , InsertSquareAction action) =>  new List.from(colors)..insert(action.color.indexColor, action.color);


List<squarePointModel> updateColorReducer(List<squarePointModel> colors , UpdateSquareAction action) =>  new List.from(colors)..replaceRange(action.start,action.end,{action.color});

List<squarePointModel> colorReducer(List<squarePointModel> colors, action) {
  if (action is AddSquareAction) {
  return addColorReducer(colors, action);
  } else if(action is RemoveSquareAction) {
  return removeColorReducer(colors, action);
  } else if( action is InsertSquareAction) {
    return insertColorReducer(colors, action);
  } else if( action is UpdateSquareAction ) {
    return updateColorReducer(colors, action);
  } else {
    return colors;

  }
}
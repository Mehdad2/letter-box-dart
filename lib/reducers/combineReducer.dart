import 'package:nytgames/models/appState.dart';
import 'package:nytgames/reducers/drawLetterReducer.dart';
import 'package:nytgames/reducers/squareReducer.dart';
import 'package:nytgames/reducers/counterReducer.dart';
import 'package:nytgames/reducers/drawPointReducer.dart';
import 'package:nytgames/reducers/wordReducer.dart';

AppState appStateReducer(AppState state, action) => new AppState(
    colorReducer(state.squareDots,action),
    drawPointReducer(state.drawPoint, action),
    drawLetterReducer(state.drawLetter, action),
    wordReducer(state.words, action),
    addCounterReducer(state.counter, action)
);
import 'package:nytgames/models/wordModel.dart';
import 'package:nytgames/actions/wordAction.dart';


List<wordModel> addWordReducer(List<wordModel> word, addWordAction action) => List.from(word)..add(action.wordItems);

List<wordModel> removeWordReducer(List<wordModel> word, action) => List.from(word)..removeLast();

List<wordModel> updateWordReducer(List<wordModel> word, UpdateWordAction action) => List.from(word)..replaceRange(action.start, action.end, {action.wordItem});


wordReducer(List<wordModel> word, action) {
  if(action is addWordAction) {
    return addWordReducer(word, action);
  } else if( action == wordsAction.removeLast) {
    return removeWordReducer(word, action);
  } else if(action is UpdateWordAction) {
    return updateWordReducer(word, action);
  } else {
    return word;
  }
}
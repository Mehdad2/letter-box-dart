import 'package:nytgames/models/drawPointModel.dart';
import 'package:nytgames/models/pointModel.dart';

class addDrawPointAction {
  drawPointModel dotsItem;
  addDrawPointAction(this.dotsItem);

}

class updateDrawPointAction {
  int start;
  int end;
  drawPointModel dotsItem;
  updateDrawPointAction(this.start, this.end, this.dotsItem);
}

enum DrawPointAction {
  removeLast
}
import 'package:nytgames/models/squarePointModel.dart';

class AddSquareAction {
  squarePointModel color;
  AddSquareAction(this.color);
}

class RemoveSquareAction {
  int indexColor;
  RemoveSquareAction(this.indexColor);
}


class InsertSquareAction {
  squarePointModel color;
  InsertSquareAction(this.color);
}

class UpdateSquareAction {
  int start;
  int end;
  squarePointModel color;
  UpdateSquareAction(this.start, this.end,this.color);
}
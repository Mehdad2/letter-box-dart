import 'package:nytgames/models/wordModel.dart';

class addWordAction {
  wordModel wordItems;
  addWordAction(this.wordItems);
}


class UpdateWordAction {
  int start;
  int end;
  wordModel wordItem;
  UpdateWordAction(this.start, this.end,this.wordItem);
}

enum wordsAction {
  removeLast
}
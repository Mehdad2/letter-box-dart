import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/components/NytDiagram.dart';
import 'package:nytgames/models/appState.dart';
import 'package:touchable/touchable.dart';
import 'package:nytgames/Utils.dart';


class NytDiagramState extends StatelessWidget {

  Utils utils = Utils();
  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    return CanvasTouchDetector(
            builder: (context) =>
                CustomPaint(painter: NytDiagram(context,utils, store))
        );

  }
}
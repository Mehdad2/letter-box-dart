import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/actions/squareAction.dart';
import 'package:nytgames/actions/drawPointAction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nytgames/actions/wordAction.dart';
import 'package:nytgames/components/NytDiagram.dart';
import 'package:nytgames/containers/NytDiagramState.dart';
import 'package:nytgames/containers/NytGameState.dart';
import 'package:nytgames/models/appState.dart';
import 'package:nytgames/models/drawPointModel.dart';
import 'package:nytgames/models/squarePointModel.dart';
import 'package:nytgames/models/wordModel.dart';
import 'package:redux/redux.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:flutter_config/flutter_config.dart';

class HomePage extends StatefulWidget {
  String title;
  HomePage(this.title);
  @override
  MyHomePage createState() => MyHomePage(this.title);

}

class MyHomePage extends State<HomePage> {
  BuildContext context;
  String title;
  bool responseOk = false;
  Set uniqueLetter = new Set();

  MyHomePage(this.title);

  void _onValidUpdateSquarePointColor(store) {
    for (var el in store.state.squareDots) {
      if (el.colors == Colors.green) {
        store.dispatch(
            UpdateSquareAction(
                el.indexColor, el.indexColor + 1, squarePointModel(
                el.indexColor, Colors.amber, 0, el.side, false)));
      }
    }
  }

  void _onValidDrawLine(store) {
    var foundDrawPoint = store.state.drawPoint.where((e) =>
    e.count != 0 && e.isValidated == false);
    if (foundDrawPoint.length > 0) {
      store.dispatch(updateDrawPointAction(
          store.state.drawPoint.length - 1, store.state.drawPoint.length,
          drawPointModel(
              store.state.drawPoint[store.state.drawPoint.length - 1].point,
              true,
              4,
              null)));
    }
  }


  Future _onValidManageWord(store) async {
    var foundLetters = store.state.words.where((e) => e.isValidated == false);
    if (foundLetters.length > 0) {
      var word = store.state.words[store.state.words.length - 1].word.join();
      var keydic = 'dict.1.1.20210112T223510Z.22cf56c25104618e.0040469fc2c66200378a6b83bf95e90713b87919';

      //FlutterConfig.get('API_DIC_KEY');
      if (word.length > 2) {
        //var url = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${keydic}&lang=en-en&text=${word}';
       // var url = 'https://api.dictionaryapi.dev/api/v2/entries/en/${word}';
        var url = 'https://api.datamuse.com/words?sp=${word}&md=d';

        // Await the http get response, then decode the json-formatted response.
        var response = await http.get(url);
        if (response.statusCode == 200) {
          var jsonResponse = convert.jsonDecode(response.body)[0];
          var wordReponse = jsonResponse['defs'];
          if (wordReponse != null && wordReponse.length > 0) {
            for (var i = 0; i < word.length; i++) {
              uniqueLetter.add(word[i]);
            }
            setState(() {
              responseOk = !responseOk;
            });
          } else {
            print('no such word');
          }
        } else {
          print('Request failed with status: ${response.statusCode}.');
        }
      } else {
        print('word too short, it should have a length  of three letter');
      }
    }
  }

  void _onValidAddWord(store) {
    store.dispatch(UpdateWordAction(
        store.state.words.length - 1, store.state.words.length, wordModel(
        store.state.words[store.state.words.length - 1].word,
        true,
        store.state.words[store.state.words.length - 1].side
    )));
  }

  void _onDeleteRemoveWord(store) {
    if (store.state.words[store.state.words.length - 1].word.length > 0) {
      store.state.words[store.state.words.length - 1].word.removeLast();
      store.state.drawPoint[store.state.drawPoint.length - 1].point
          .removeLast();
      uniqueLetter.remove(uniqueLetter.elementAt(uniqueLetter.length - 1));
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    if (responseOk) {
      _onValidUpdateSquarePointColor(store);
      _onValidDrawLine(store);
      _onValidAddWord(store);
      showInSnackBar('Nice');
      setState(() {
        responseOk = !responseOk;
      });
      if (uniqueLetter.length == 12) {
        Future.delayed(const Duration(seconds: 1), () =>   _showMyDialog(context, store));

      }
    }


    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(this.title),
        ),
        body: Column(

            children: [

              Center(
                  child: Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 40, vertical: 40),
                      width: 600,
                      height: 400,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white)),
                      child: NytGame(this.responseOk)
                  )
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center,children: store.state.words.map((e)=>  Text(e.word.join('') + ' - ')).toList()),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonBar(
                      alignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: [new RaisedButton(color:Colors.amber[300], child: Text("Enter"),
                          onPressed: () {
                            _onValidManageWord(store);
                          }
                      ),
                      ]
                  ),
                  ButtonBar(
                      alignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: [new RaisedButton( color:Colors.red[300] ,child: Text("Delete"),
                          onPressed: () {
                            _onDeleteRemoveWord(store);
                          }

                      ),
                      ]
                  ),
                ],
              ),


            ]
        )
    );
  }


  void _showMyDialog(context, store) {
    showDialog(
        context: context,
        barrierDismissible: false,
        //this means the user must tap a button to exit the Alert Dialog
        builder: (BuildContext context) => AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(1.0, 10.0, 1.0, 5.0),
              scrollable: true,
              title: Text('Level 1', style: TextStyle(color: Colors.amber), textAlign: TextAlign.center),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Container(
                    padding: EdgeInsets.symmetric(
                    horizontal: 0, vertical: 0),
                  width: 500,
                  height: 300,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white)),
                  child:  NytDiagramState()
              ),
                    Text('you solved it in only ${store.state.words.length} words', textAlign: TextAlign.center),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  color: Colors.amber,
                  child: Text('Retry', style:  TextStyle(color: Colors.white)),
                  onPressed: () {
                    //Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  color: Colors.green[200],
                  child: Text('Next', style:  TextStyle(color: Colors.white)),
                  onPressed: () {
                    //Navigator.of(context).pop();
                  },
                ),
              ],
            )
    );
  }




  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value),
      behavior:  SnackBarBehavior.floating,
      backgroundColor: Colors.amber,
      elevation: 10.0,
    ));
  }
}
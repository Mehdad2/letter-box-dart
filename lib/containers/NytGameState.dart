import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/components/NytGamePainter.dart';
import 'package:nytgames/models/appState.dart';
import 'package:redux/redux.dart';
import 'package:touchable/touchable.dart';
import 'package:nytgames/Utils.dart';

class NytGame extends StatefulWidget {
  final responseOk;
  NytGame(this.responseOk);
  @override
  _NytGameState createState() => _NytGameState();
}

class _NytGameState extends State<NytGame> {
  bool tap = false;
  bool responseOk = false;
  Utils utils = Utils();
 // Store<AppState> store;


  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);

    return GestureDetector(
      onTapDown: (details) {
        setState(() {
          tap = !tap;
        });
      },
      child:CanvasTouchDetector(
        builder: (context) =>
        CustomPaint(painter: NytGamePainter(context, utils, this.tap, store, widget.responseOk))
    )
    );

  }
}
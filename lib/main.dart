import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nytgames/containers/MyHomePage.dart';
import 'package:nytgames/models/appState.dart';
import 'package:nytgames/reducers/combineReducer.dart';
import 'package:redux/redux.dart';
import 'package:flutter_config/flutter_config.dart';
void main() async{
  //WidgetsFlutterBinding.ensureInitialized(); // Required by FlutterConfig
 // await FlutterConfig.loadEnvVariables();
  final store = new Store<AppState>(appStateReducer, initialState: new AppState.initialState());
  runApp(MyApp(store: store));

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Store<AppState> store;

  MyApp({Key key,this.store}): super(key: key);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child:MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: HomePage('Letter Boxed Game'),
    )
    );
  }
}
